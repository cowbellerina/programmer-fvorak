# Programmer Fvorak

Programmer Dvorak Keyboard Layout with a Finnish twist

![figure](./screenshot/programmer-fvorak-keyboard-layout.png "Programmer Fvorak Keyboard Layout")

This keyboard layout extends the Programmer Dvorak layout with the letters `ä` and `ö`. It leverages the extra key
("`key`") found in the European-style keyboards located in the lower left corner of the keyboard.

- Pressing`key` produces "ä"
- Pressing`Shift` + `key` produces "ö"
- Pressing`Alt` + `key` produces "Ä"
- Pressing `Alt` + `Shift` + `key` produces "Ö"

## Prerequisites

- Ubuntu 18.04 (Bionic Beaver)

It's highly likely the keyboard layout works in earlier versions of Ubuntu (>= 8.10) but it has only been tested in
Ubuntu 18.04.

## Installation

### Ubuntu 18.04 (Bionic Beaver)

1. Clone this repository
2. Copy the keyboard layout under the system keyboard layouts directory
3. Update the system keyboard layouts configuration
4. Restart the computer
5. Add the keyboard layout as an input source

#### 1. Clone the repository

```
git clone git@gitlab.com:cowbellerina/programmer-fvorak.git
cd programmer-fvorak
```

#### 2. Copy the keyboard layout under the system keyboard layouts directory

In X11 systems, the keyboard layouts are kept under `/usr/share/X11/xkb/symbols/`.

```
sudo cp fvp /usr/share/X11/xkb/symbols/
```

#### 3. Update the system keyboard layouts configuration

Make the keyboard layout discoverable by adding it to the system keyboard layouts configuration file.

- Open `/usr/share/X11/xkb/rules/evdev.xml` to an editor of your choice
- Add the following XML inside the `<layoutList>` section
- Save the file

```xml
<layout>
    <configItem>
        <name>fvp</name>
        <shortDescription>Programmer Fvorak</shortDescription>
        <description>Programmer Fvorak</description>
        <languageList>
            <iso639Id>fin</iso639Id>
        </languageList>
    </configItem>
    <variantList/>
</layout>
```

#### 4. Restart the computer

#### 5. Add the keyboard layout as an input source

The keyboard layout is available under Finnish keyboard layouts.

- Open  `Settings` › `Region & Language`
- Under "Input Sources" click the `+` button
- Click the `...` (More) button to reveal all the options and the search field
- Select "Programmer Fvorak" under "Finnish"

![figure](./screenshot/5-add-an-input-source.png "Add an Input Source window")

## Acknowledgements / thanks

- [Custom Keyboard in Linux/X11](http://people.uleth.ca/~daniel.odonnell/Blog/custom-keyboard-in-linuxx11) by Daniel
Paul O'Donnell for the instructions
- [ArkkuDvorak](http://arkku.com/dvorak/) by Kimmo Kulovesi for the original idea
- [Programmer Dvorak Keyboard Layout](https://www.kaufmann.no/roland/dvorak/) by Roland Kaufmann
